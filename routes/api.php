<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::post('/v1/getBookingByAttr',  function (){echo "FUHHH00";});


//Route::post('/v1/getBookingByAttr', array('as'=>'Booking Search by Attribute', 'uses'=>'BookingController@getBookingByAttr'));




//Route::group(['prefix' => '/v1','middleware' => ['secure']], function() {
 Route::group(['prefix' => '/v1'], function() {

    Route::get('/bookings', array('as'=>'Booking List', 'uses'=>'BookingController@bookings'));
	Route::get('/bookings/{id}', array('as'=>'Booking Edit', 'uses'=>'BookingController@getEditBookingDetail'));
	Route::put('/bookings/{id}', array('as'=>'Booking Update', 'uses'=>'BookingController@updateBooking'));
	Route::post('/bookings', array('as'=>'Create Booking', 'uses'=>'BookingController@createBooking'));
    Route::delete('/bookings/{id}', array('as'=>'Delete Booking', 'uses'=>'BookingController@deleteBooking'));
    Route::get('/booking/status/{status}/{id}', array('as'=>'update Booking status', 'uses'=>'BookingController@updateBookingStatus'));

	Route::post('/get/booking/attr', array('as'=>'Booking Search by Attribute', 'uses'=>'BookingController@getBookingByAttr'));
	Route::get('/deleteBooking/{id}', array('as'=>'Delete Booking', 'uses'=>'BookingController@deleteBooking'));

    Route::get('/booking/{id}/details', array('as'=>'Booking Details', 'uses'=>'BookingController@getBookingDetail'));
    Route::get("/bookings/{start}/{end}", array('as'=>'Booking Details', 'uses'=>'BookingController@getBookings'));

 });
