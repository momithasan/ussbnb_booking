<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::get('/', array('as'=>'Booking List', 'uses'=>'BookingController@bookings'));
 Route::get('/booking/create', array('as'=>'Create Booking', 'uses'=>'BookingController@createBooking'));
 Route::get('/booking/edit/{id}', array('as'=>'Booking Edit', 'uses'=>'BookingController@getEditBookingDetail')); 