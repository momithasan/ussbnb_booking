-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.11-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table uss_bnb_booking.bookings
CREATE TABLE IF NOT EXISTS `bookings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  `booking_status_id` tinyint(1) unsigned DEFAULT NULL COMMENT '1:Confirmed,2:Pending,3:canceled',
  `from` timestamp NULL DEFAULT NULL,
  `till` timestamp NULL DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(11,2) unsigned DEFAULT NULL,
  `seats` int(10) unsigned DEFAULT NULL,
  `guest_first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guest_last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guest_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guest_company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guest_company_reg_nr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guest_company_vat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guest_company_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guest_company_contacts` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `checked_in_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table uss_bnb_booking.bookings: ~4 rows (approximately)
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;
INSERT INTO `bookings` (`id`, `client_id`, `room_id`, `booking_status_id`, `from`, `till`, `comment`, `price`, `seats`, `guest_first_name`, `guest_last_name`, `guest_email`, `guest_company_name`, `guest_company_reg_nr`, `guest_company_vat`, `guest_company_address`, `guest_company_contacts`, `checked_in_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(11, 10, 17, 1, '2020-03-10 20:09:04', '2020-03-10 20:19:04', NULL, 33.20, NULL, 'Litu', 'Hasan', 'litu.momit@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-11 02:04:12', '2020-03-16 10:50:02', NULL),
	(12, 10, 17, 1, '2020-03-10 20:09:04', '2020-03-10 20:19:04', NULL, 44.00, NULL, 'Litu', 'Hasan', 'litu.momit@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-11 10:33:47', '2020-03-11 10:44:25', NULL),
	(13, 10, 18, 1, '2020-03-10 20:09:04', '2020-03-10 20:19:04', NULL, 11.00, NULL, 'Litu', 'Hasan', 'litu.momit@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-13 09:23:20', '2020-03-13 09:23:20', NULL),
	(14, 10, 19, 1, '2020-03-10 20:09:04', '2020-03-10 20:19:04', NULL, 11.00, NULL, 'Litu', 'Hasan', 'litu.momit@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-16 10:41:09', '2020-03-16 10:41:09', NULL);
/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;

-- Dumping structure for table uss_bnb_booking.booking_service
CREATE TABLE IF NOT EXISTS `booking_service` (
  `booking_id` int(10) unsigned NOT NULL,
  `service_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned DEFAULT 0,
  `total_amount` int(10) unsigned DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1:active,0:in-active',
  `comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  KEY `booking_service_booking_id_foreign` (`booking_id`),
  KEY `booking_service_service_id_foreign` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table uss_bnb_booking.booking_service: ~0 rows (approximately)
/*!40000 ALTER TABLE `booking_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking_service` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
